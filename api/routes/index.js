const router = require('express').Router();

const formidable = require('formidable');
const { awsRegion, awsBucketName } = require('../config/vars');
const AWS = require('aws-sdk');
const path = require('path');
const fs = require('fs');
const async = require('async');
//const uuid = require('uuid');

AWS.config.loadFromPath(path.join(__dirname, "../config/aws.json"));

const s3 = new AWS.S3({
    apiVersion: '2019-09-29',
    httpOptions: {
        timeout: 240000
    }
}); 

router.get('/home', (req, res) => {
	res.render("home");
});

router.post('/send', (req, res) => {
	let form = new formidable.IncomingForm();

    form.parse(req, (err, fields, files) => {
      if (err) return res.json({stauts: false, erro: err});

      const oldpath = files.filetoupload.path;
      const newpath = path.dirname(require.main.filename) +  "/public/images/" + files.filetoupload.name;
      
      fs.rename(oldpath, newpath, (err) => {
        if (err) return res.json({stauts: false, erro: err}); 
      });

      uploadToS3(newpath, files.filetoupload.name, (err, url) => {
         if (err) return res.json({stauts: false, erro: err}); 

         res.json({stauts: true, url: url, mensagem: "Imagem enviada com sucesso!"}); 
      });
    });
});

function uploadToS3(path, name, callback){
    fs.readFile(path, (err, data) => {
        if(!err){
            const params = {
                Bucket: awsBucketName,
                Key: name,
                Body: data
            };

            s3.putObject(params, (err, data, next) => {
                if (err) callback(err, null);
                
                let url = `https://${awsBucketName}.s3-sa-east-1.amazonaws.com/${name}`;
                callback(null, url);
            });
         }
    })
}

module.exports = router;