const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const routes = require('./routes');

app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: false, parameterLimit: 1000000}));
app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.set('views', './views');

app.use('', routes);

module.exports = app;