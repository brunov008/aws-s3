const app = require('./api/express.js');

const port = process.env.PORT || 3031;

app.listen(port, () => console.info(`server started on port ${port}`));

module.exports = app;